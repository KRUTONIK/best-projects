import dataclasses


@dataclasses.dataclass
class Genre:
    id: int
    name: str
    description: str


@dataclasses.dataclass
class Anime:
    id: int
    name: str
    description: str
    genre_id: int
    genre: str
import database.connection
import database.models


def get_genres():
    with database.connection.connect() as db:
        return [
            database.models.Genre(*row)
            for row in db.execute("SELECT * FROM genres").fetchall()
        ]


def get_animes(genre_id):
    with database.connection.connect() as db:
        return [
            database.models.Anime(*row)
            for row in  db.execute(
                "SELECT * FROM animes WHERE genre_id = ?", [genre_id]
            ).fetchall()
        ]


def get_genre_name(genre_id):
    with database.connection.connect() as db:
        genre = db.execute(
            "SELECT * FROM genres WHERE id = ?", [genre_id]
            ).fetchone()

    return database.models.Genre(*genre)


def get_anime(anime_id):
    with database.connection.connect() as db:
        anime = db.execute(
            "SELECT * FROM animes WHERE id = ?", [anime_id]
            ).fetchone()

    return database.models.Anime(*anime)
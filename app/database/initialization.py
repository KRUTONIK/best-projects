import sqlite3
import database.connection as db_con


SQL_SCHEMA = """
PRAGMA foreign_keys = ON;

DROP TABLE IF EXISTS genres;
DROP TABLE IF EXISTS animes;

CREATE TABLE genres (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    description TEXT NOT NULL
);

CREATE TABLE animes (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    genre_id INTEGER NOT NULL,
    genre TEXT NOT NULL
);
"""


def init_db():
    with db_con.connect() as db:
        db.executescript(SQL_SCHEMA)
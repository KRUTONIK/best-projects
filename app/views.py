import flask
import database.utils as db_utils


views = flask.Blueprint("views", __name__)


@views.route("/")
def main():
    return flask.render_template("main.html", genres=db_utils.get_genres())


@views.route("/<int:genre_id>")
def animes(genre_id):
    db_utils.get_animes(genre_id)
    return flask.render_template("animes.html", genre=db_utils.get_genre_name(genre_id), animes=db_utils.get_animes(genre_id))


@views.route("/anime/<int:anime_id>")
def anime(anime_id):
    return flask.render_template("anime.html", anime=db_utils.get_anime(anime_id))
